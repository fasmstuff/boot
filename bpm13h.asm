include "mbr/head.inc"
include "screen/mode13h.inc"
include "pmode/pmode.inc"

os:
.entry:
;        mov ecx,100'0000h
;@@:
;        mov [10'0000h+ecx*4],ecx
;        loop @b
        mov edx,viewport.base
        ;inc ecx
        mov dword[edx+viewport.x],ecx
        mov dword[edx+viewport.y],2049;ecx
        mov edi,kb.base

.loop:
        call refresh
        call keycases
        call viewport
        dec dword[edx+viewport.x]

        mov edi,print.base
        mov dword[edi+print.x],320/2;ecx
        mov dword[edi+print.y],200/2;ecx

        inc byte[edi+print.c]
        mov esi,.string
        call print.string

        call kb.input
        jmp .loop


.string db "HELLO",0
lib:
include "keyboard/kb.inc"
include "keyboard/input.inc"
include "keyboard/keycases.inc"
include "graphic/viewport.inc"
include "graphic/put.inc"
include "graphic/print.inc"
include "screen/refresh.inc"

mbrsignature:
include "mbr/aa55.inc"
