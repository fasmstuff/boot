include "mbr/head.inc"
include "screen/mode3.inc"
include "pmode/pmode.inc"

os:
        call kb.input
        je os
        call kb.scan
;        mov ecx,10
@@:
        mov esi,[.ptr]
        call print.hexaview
        add [.ptr],16
        mov al,' '
        call print.char
        mov eax,[kb.base+kb.code]
        call print.dword
        call print.scroll
        call kb.txtview
;        loop @b

        jmp os

.ptr    dd 0

lib:
include "keyboard/kb.inc"
include "keyboard/input.inc"
include "keyboard/scan.inc"
include "keyboard/txtview.inc"
include "text/print.inc"

mbrsignature:
include "mbr/aa55.inc"