print:
.char:
        pushad
        movzx esi,al
        shl esi,3
        add esi,bioschars
        mov edi,.base
        mov eax,[edi+.x]
        mov ebx,[edi+.y]
        mov ch,8
.loadline:
        mov dh,[esi]
        mov dl,80h
.nextdot:
        test dh,dl
        je @f
        mov cl,[edi+.c];31
        call put.pixel
@@:
        inc eax
        shr dl,1
        jne .nextdot
        inc ebx
        mov eax,[edi+.x]
        inc esi
        dec ch
        jne .loadline
        add dword[edi+.x],8
        popad
        ret

.string:
;esi asciiz string
@@:
        lodsb
        or al,al
        je @f
        call .char
        jmp @b
@@:
        ret


.base = 10200h
.x    = 0
.y    = 4
.c    = 8

bioschars = 0ffa6eh
.xl=16
.yl=16
