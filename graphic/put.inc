put:
.base=10000h
.x=0
.y=4
.xl=8
.yl=12
.c=16

.box:
        pushad
        mov esi,.base
        mov ecx,[esi+.c]
        mov ebx,[esi+.y]
        mov edx,ebx
        add edx,[esi+.yl]
        mov edi,[esi+.x]
        add edi,[esi+.xl]
.newline:
        mov eax,[esi+.x]
@@:
        call .pixel
        inc eax
        cmp eax,edi
        jl @b
        inc ebx
        cmp ebx,edx
        jl .newline
        popad
        ret

;.hline:
;        mov eax,.base
;        xor ebx,ebx
;        mov dword[eax],ebx
;        inc ebx
;        mov dword[eax+12],ebx
;        mov dword[eax+8],screen.xl
;        call .box
;        ret

;.vline:
;        mov eax,.base
;        xor ebx,ebx
;        mov dword[eax+4],ebx
;        inc ebx
;        mov dword[eax+8],ebx
;        mov dword[eax+12],screen.yl
;        call .box
;        ret

;        xor ebx,ebx
;        mov eax,[.x]
;        mov cl,[.c]
;@@:
;        call .pixel
;        inc ebx
;        cmp ebx,screen.yl
;        jl @b
;        ret

.pixel:
        push ebx
        cmp eax,screen.xl
        jae @f
        cmp ebx,screen.yl
        jae @f
        imul ebx,screen.xl
        mov [screen.base+eax+ebx],cl
;        xor [screen.base+eax+ebx],cl
@@:
        pop ebx
        ret

