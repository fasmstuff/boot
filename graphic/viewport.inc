viewport:
        pushad
        mov edx,put.base
        mov ebx,[.base+.y]
        mov esi,ebx
        and ebx,.yl-1
        neg ebx
        ;sar esi,.granularity
        lea esi,[esi*5]
        shl esi,6
;        shl esi,16
.newline:
;        movzx eax,byte[.base+.off]
        mov eax,[.base+.x]
        mov edi,eax
        sar edi,.granularity
        and eax,.xl-1
        neg eax
@@:
        mov cl,[esi+edi]
        mov [edx+put.c],cl
        mov [edx+put.x],eax
        mov [edx+put.y],ebx
        mov dword[edx+put.xl],.xl
        mov dword[edx+put.yl],.yl

        call put.box
        add eax,.xl
        inc edi
        cmp eax,screen.xl
        jl @b
        add ebx,.yl
        add esi,320;160;10000h
        cmp ebx,screen.yl
        jl .newline
        popad
        ret
.base=10100h
.granularity=0
.x=0
.y=4
.off=8
.xl=(1 shl .granularity)
.yl=(1 shl .granularity)
