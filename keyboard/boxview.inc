.boxview:
;display a ram window;
;the ram is a 65536 bytes per side square
;the window is a 320*200 viewport with 16 pixels wide squares
;x and y coordinates of the viewport are passed by registers
;and gives the viewport a granularity of 1/16th of byte


        mov esi,7c00h;kb.base+kb.array
        mov edi,put.base
        mov dword[edi+put.xl],16
        mov dword[edi+put.yl],16
        call put.cls
        xor edx,edx
@@:
        movzx eax,dl
        mov ebx,eax
        shl al,4
        and bl,70h
        mov cl,[esi+edx]
        mov [edi+put.x],eax
        mov [edi+put.y],ebx
        mov byte[edi+put.c],cl
        push edx edi esi
        call put.box
        pop esi edi edx
        inc dl
        jns @b
        ret
