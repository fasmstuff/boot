.input:
;return condition equal if no key, not equal if key
        pushad
        in al,64h
        test al,1
        je @f
        in al,60h
        cmp al,0e0h
        je @f
        cmp al,0e1h
        je @f
        shl dword[edi+.code],8
        mov [edi+.code],al
        mov al,[edi+.code]
        movzx ebx,al
        and bl,7fh
        shr al,7
        xor al,1
        mov [edi+.array+ebx],al
@@:
        popad
        ret
