kb:
.base   = 1000h
.array  = 0
.code   = 128

.escape=1
.1=2
.2=3
.3=4
.4=5
.5=6
.6=7
.7=8
.8=9
.9=10
.0=11

.up=48h
.down=50h
.left=4bh
.right=4dh
.pageup=49h
.pagedown=51h
.begin=47h
.end=4fh
.ctrl=1dh
.alt=38h
.space=39h
.enter=1ch
.backspace=0eh
