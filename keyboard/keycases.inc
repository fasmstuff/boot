keycases:
;        mov ebx,kb.base+kb.array
        cmp byte[edi+kb.array+kb.right],0
        je @f
        inc dword[edx+viewport.x]
@@:
        cmp byte[edi+kb.array+kb.left],0
        je @f
        dec dword[edx+viewport.x]
@@:
        cmp byte[edi+kb.array+kb.down],0
        je @f
        inc dword[edx+viewport.y]
@@:
        cmp byte[edi+kb.array+kb.up],0
        je @f
        dec dword[edx+viewport.y]
@@:
        ret
