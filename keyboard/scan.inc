.scan:
        mov al,[.base+.code]
        movzx ebx,al
        and bl,7fh
        shr al,7
        xor al,1
        mov [.base+.array+ebx],al
        ret
