txtview:
        xor ebx,ebx
        mov al,bl
@@:
        mov ah,[kb.base+kb.array+ebx]
        shl ah,4
        mov edx,ebx
        mov [screen.base+edx*screen.bpp],ax
        inc bl
        jnl @b
        ret
