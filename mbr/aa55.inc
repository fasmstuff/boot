;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
free =  510-(padding-$$)        ; define "free" bytes count
padding rb free                 ; reserve "free" bytes to make line below at offset 510
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        dw 0aa55h               ; magic number boot mark, used by bios to test if valid boot sector
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

include "macros.inc"
memsize free

