macro bargraphword w,c {
b = (w)/16
repeat b
    display c
end repeat
    display 13,10
}

macro debugword w,s {
d0 = '0' + (w) shr 12 and 0fh
d1 = '0' + (w) shr 8 and 0fh
d2 = '0' + (w) shr 4 and 0fh
d3 = '0' + (w) and 0fh

if d0>'9'
        d0=d0+7
end if

if d1>'9'
        d1=d1+7
end if

if d2>'9'
        d2=d2+7
end if

if d3>'9'
        d3=d3+7
end if

display d0,d1,d2,d3,'h '
display s,13,10
}

macro memsize m {
      bargraphword m,'='
      debugword m,'free bytes'
      bargraphword 510-m,'='
      debugword 510-m,'used bytes'
}
