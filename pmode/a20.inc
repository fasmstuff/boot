a20enable:
;        pusha
;        cli                                    ; Disable all irqs
        cld

        mov   al,0ffh                           ; Mask all irqs
        out   0a1h,al
        out   021h,al

@@:     in    al,064h                          ; Enable A20
        test  al,2                             ; Test the buffer full flag
        jnz   @b                               ; Loop until buffer is empty

        mov   al,0d1h                          ; Keyboard: write to output port
        out   064h,al                          ; Output command to keyboard

@@:     in    al,064h
        test  al,2
        jnz   @b                               ; Wait 'till buffer is empty again

        mov   al,0dfh                          ; keyboard: set A20
        out   060h,al                          ; Send it to the keyboard controller
        mov   cx,14h
@@:                                            ; this is approx. a 25uS delay to wait
        out   0edh,ax                          ; for the kb controler to execute our
        loop  @b                               ; command.
;        sti
;        popa
;        ret
