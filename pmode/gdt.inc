gdt:    dw 0                    ; in order to align dword part of pseudo desciptor on dword boundary
.size   dw @f-gdt-1             ; word part of pseudo desciptor, size of gdt in bytes
.linear dd gdt                  ; dword part of pseudo descriptor, linear base adress
.code=$-gdt                     ; first entry in gdt (8*1)
dw -1,0                         ;   4Gbytes, start at linear 0
db 0,9ah,0cfh,0                 ;   granularity = 64Kbytes, code segment, ring 0, read only,etc...
.data=$-gdt                     ; second entry in gdt (8*2)
dw -1,0                         ;   4Gbytes, start at linear 0
db 0,92h,0cfh,0                 ;   granularity = 64Kbytes, data segment, ring 0, read/write,etc...
@@:                             ; used for gdt.size calculation
