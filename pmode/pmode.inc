include "pmode/fasta20.inc"
rmode:
        cli                     ; disable interrupts
        lgdt fword[cs:gdt.size] ; load the gdt from [cs:gdt] 6 bytes pseudo descriptor
        mov eax,cr0             ; equivalent to "or cr0,1"
        or al,1                 ;   switches the CPU in protected mode-
        mov cr0,eax             ;   protected mode enable
        jmp gdt.code:pmode      ; equivalent to "mov cs,gdt.data" + "mov ip,pmode"
include "gdt.inc"
pmode:                          ;   the first instruction right after pm enable
        use32                   ; code below is 32 bits
        mov ax,gdt.data         ;
        mov ds,ax               ; make ds = .data entry in gdt, flat linear adress space
