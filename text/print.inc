;print library
print:
.scroll:
        mov ebx,screen.base+screen.xl*screen.bpp*3
@@:
        mov eax,[ebx]
        mov [ebx-(screen.xl*screen.bpp)],eax
        add ebx,4
        cmp ebx,screen.base+screen.xl*screen.bpp*(screen.yl+1)
        jl @b
        mov dword[print.ptr],screen.base+screen.xl*screen.bpp*screen.yl
        ret

.char:
;al char
        mov ah,[.color]
        mov edi,[.ptr]
        stosw
        cmp edi,screen.base+screen.xl*screen.bpp*(screen.yl+1)
        jl @f
        call .scroll
@@:
        mov [.ptr],edi
        ret

.string:
;esi asciiz string
@@:
        lodsb
        or al,al
        je @f
        call .char
        jmp @b
@@:
        ret

.byte:
;al byte
        mov bl,al
        shr al,4
        call @f
        xchg bl,al
        and al,0fh
@@:
        cmp al,10
        jl @f
        add al,'A'-'9'-1
@@:
        add al,'0'
        call .char
        ret

.word:
;ax word
        push ax
        shr ax,8
        call .byte
        pop ax
        call .byte
        ret

.dword:
;eax dword
        push eax
        shr eax,16
        call .word
        pop eax
        call .word
        ret

.hexaview:
;esi start
;ecx lines
        mov eax,esi
        call .dword

        mov al,' '
        call .char

        mov edx,16
.hex:
        lodsb
        call .byte
        dec edx
        jne .hex

        mov al,' '
        call .char

        mov edx,16
        sub esi,edx
.ascii:
        lodsb
        call .char
        dec edx
        jne .ascii
        ret

.color  db 07fh
.ptr    dd screen.base+screen.xl*screen.bpp*screen.yl

